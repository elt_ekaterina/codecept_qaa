const assert = require('assert');
Feature('auth');

Scenario('Авторизоваться с логин/пароль через API', async ({ I }) => {
    const request = await I.sendPostRequest('/login', { "username": "demo", "password": "demo" });
    assert.deepEqual(request.status, 200)
});

Scenario('Авторизоваться с логин/пароль через UI', async ({ I, mainPage, currentPage }) => {
    I.amOnPage();
    I.say('Начинаем авторизацию');
    await mainPage.login('demo', 'demo');
    const profileNameText = await currentPage.getProfileName();
    assert.deepEqual(profileNameText, 'demo')
});
